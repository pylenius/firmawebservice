﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TestWebService
{
    public partial class Default : System.Web.UI.Page
    {
        private static int count = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            GridView1.DataSource = PushService.ClientURIList;
            GridView1.DataBind();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            new PushService().PushRawData("Hello from Web!");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            new PushService().PushTileUpdate("New order", count++ , Page.ResolveUrl("tile" + count % 3 + ".png"));
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            new PushService().PushToast("New order", string.Format(("{0} new orders."), count), "/MainPage.xaml");
        }
    }
}