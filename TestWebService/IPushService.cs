﻿using System;
using System.ServiceModel;

namespace TestWebService
{
    [ServiceContract]
    public interface IPushService
    {
        [OperationContract]
        void SubscribeMyPhone(Guid phoneID, string channelUri);

        [OperationContract]
        void PushRawData(string rawMessage);

        [OperationContract]
        void PushToast(string toastTitle, string toastMessage, string uri);

        [OperationContract]
        void PushTileUpdate(string tileTitle, int tileCount, string tileImageUri);
    }
}
