﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace TestWebService
{
    [DataContract]
    public class Customer
    {
        [DataMember]
        public int CustomerID { get; set; }
        [DataMember]
        public string Name { get; set; }
    }

    [DataContract]
    public class Order
    {

        [DataMember]
        public int Number { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public decimal Total { get; set; }

        [DataMember]
        public int CustomerID { get; set; }
    }

    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        List<Customer> GetCustomers();

        [OperationContract]
        List<Order> LatestOrders(DateTime since);

        [OperationContract]
        Order SaveOrder(Order order);

        [OperationContract]
        Order GetOrder(int number);
    }
}
