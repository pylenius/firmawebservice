﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace TestWebService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    public class Service1 : IService1
    {
        static private Dictionary<int, Customer> customers;
        static private List<Order> orders;

       public Service1()
        {
            customers = new Dictionary<int,Customer>() {
                {0, new Customer() { CustomerID = 0, Name = "Arska" } },
                {1, new Customer() { CustomerID = 1, Name = "Jokke" } },
                {2, new Customer() { CustomerID = 2, Name = "Jake" } },
                {3, new Customer() { CustomerID = 3, Name = "Esa" } }
            };

            if (orders == null)
            orders = new List<Order>() { 
                new Order() { Date = new DateTime(2012,1,1), Number = 1, Total = 1000, CustomerID = 0 },
                new Order() { Date = new DateTime(2012,1,2), Number = 2, Total = 1000, CustomerID = 0 },
                new Order() { Date = new DateTime(2012,1,3), Number = 3, Total = 2000, CustomerID = 1 },
                new Order() { Date = new DateTime(2012,1,4), Number = 4, Total = 3000, CustomerID = 2 },
                new Order() { Date = new DateTime(2012,1,5), Number = 5, Total = 3000, CustomerID = 3 }
            }; 

  

        }

        public void DoWork()
        {
        }


        public List<Order> LatestOrders(DateTime since)
        {
            return orders.Where(i => i.Date > since).ToList();
        }

        public List<Customer> GetCustomers()
        {
            return customers.Values.ToList();
        }


        public Order GetOrder(int number)
        {
            return orders.FirstOrDefault(i => i.Number == number);
        }


        public Order SaveOrder(Order order)
        {
            if (order.Number > 0)
            {
                orders.RemoveAll(i => i.Number == order.Number);
                orders.Add(order);
                new PushService().PushToast("Order updated", string.Format("Order number {0}.", order.Number),
                                            string.Format("/DetailsPage.xaml?selectedItem={0}", order.Number));
            }
            else
            {
                order.Number = orders.Count + 1;
                orders.Add(order);
                new PushService().PushToast("New order", string.Format("Order number {0}.", order.Number),
                                            string.Format("/DetailsPage.xaml?selectedItem={0}", order.Number));
            }
            return order;
        }
    }
}
